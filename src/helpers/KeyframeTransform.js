import Transform from './Transform'

/**
 * A child of Transform that adds the ability to clone other transform
 * Objects and to interpolate between them.
 */
class KeyframeTransform extends Transform {
  /**
    * @constructor
    * @param {Transform} copyMe - A transform to copy (must always be provided)!
    */
  constructor (copyMe) {
    // Inherit all the stuff from parent mesh
    super(copyMe._meshObj)

    // Copy the core properties from the other Transform object
    this._position.copy(copyMe._position)
    this._rotation.copy(copyMe._rotation)
    this._scale.copy(copyMe._scale)
    this._pivotPoint.copy(copyMe._pivotPoint)
  }

  /**
   * Create a new KeyframeTransformation that is the linear interpolation
   * of 'this' with Btrans using 'alpha'.
   * @this {KeyframeTransform} The 'previous' transform in the pair.
   * @param {KeyframeTransform} Btrans The 'next' transform in the pair.
   * @param {number} alpha A value between 0 and 1 used for interpolation.
   * @return {KeyframeTransform} The result of linearly interpolating
   */
  lerp (Btrans, alpha) {
    // Clone the current transform
    let interp = new KeyframeTransform(this)

    // TODO: Create a new inbetween frame using linear interpolation
    //  - 'this' is the previous key frame & Btrans is the next key frame
    //  - 'alpha' is the percent of Btrans that should be in the result
    //  - Update the values of 'interp' to be a combination of 'this' and Btrans
    //  - Use the equation we derived in class and apply it to the x, y, and z
    //    coordinates of position, rotation, scale, and pivotpoint.
    //  - Make sure to interpolate all 12 values individually.
    //  - You may NOT use THREE.Vector3.lerp(); you must write it yourself!

    // var range = Btrans.pivot.x - this.pivot.x;
    // var alpha = (requestedFrameNum - prevFrameNum) / (nextFrameNum - prevFrameNum);
    // alpha * (nextFrameNum - prevFrameNum) = (requestedFrameNum - prevFrameNum)
    // alpha * (nextFrameNum - prevFrameNum) + prevFrameNum = (requestedFrameNum)
   
    
    interp.setPivot(
      (alpha * (Btrans.pivot.x - this.pivot.x) + this.pivot.x),
      (alpha * (Btrans.pivot.y - this.pivot.y) + this.pivot.y),
      (alpha * (Btrans.pivot.z - this.pivot.z) + this.pivot.z)
    );
    
    interp.setRotation(
      (alpha * (Btrans.rotation.x - this.rotation.x) + this.rotation.x),
      (alpha * (Btrans.rotation.y - this.rotation.y) + this.rotation.y),
      (alpha * (Btrans.rotation.z - this.rotation.z) + this.rotation.z)
    );

    interp.setScale(
      (alpha * (Btrans.scale.x - this.scale.x) + this.scale.x),
      (alpha * (Btrans.scale.y - this.scale.y) + this.scale.y),
      (alpha * (Btrans.scale.z - this.scale.z) + this.scale.z)
    );

    interp.setPosition(
      (alpha * (Btrans.position.x - this.position.x) + this.position.x),
      (alpha * (Btrans.position.y - this.position.y) + this.position.y),
      (alpha * (Btrans.position.z - this.position.z) + this.position.z)
    );


     // The formula: v0 * (1.0 - step) + v1 * step
    // The more efficient formula: v0 + (v1 - v0) * step
    // Precise method, which guarantees v = v1 when t = 1.
      // float lerp(float v0, float v1, float t) {
      //   return (1 - t) * v0 + t * v1;
      // }
    

    // Return the interpolated clone
    return interp
  }
}

export default KeyframeTransform
