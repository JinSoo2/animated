// Disable type-checking as it reports incorrect errors
// @ts-nocheck

// Will use the humanoid Factory
import HumanoidFactory from './HumanoidFactory'

// Importing three to use degToRad
import * as THREE from 'three'

/**
 * A class to animate the humanoid mesh.
 **/
class AnimationFactory {
  /**
   * Create a new humanoid factory object.
   **/
  constructor() {
    this._humanoidMaker = new HumanoidFactory()
  }

  /**
   * Extract references to nodes in the hierarchy that contain a particular sub-name
   * in their name parameter.
   *
   * @param {AnimatableMesh} humanoid The hierarchical mesh to Traverse
   * @param {string} subName The name to match and extract from the hierarchy
   * @return {Array} An array of references to meshes in the hierarchy that contain
   *    the specified subName in their name parameter.
   **/
  extractNamedNodes(humanoid, subName) {
    // An array to hold the extracted node
    let nodes = []

    // Traverse the entire hierarchy
    humanoid.traverse((node) => {
      // If the name contains the given sub-name, then grab it
      // Note: we ignore ones with 'Geom' because those are from isolateScale
      if (node.name.includes(subName) && !node.name.includes('Geom')) {
        nodes.push(node)
      }
    })

    // Return an array of the extracted nodes
    return nodes
  }

  generateWalk() {
    // Create a new humanoid object using your factory
    let humanoid = this._humanoidMaker.generateMesh()
    humanoid.name = `Walking ${humanoid.name}`

    // TODO: Make it walk. Below is an example of how to code this
    // Keep in mind, your y and z values may need to be different.

    var neck = this.extractNamedNodes(humanoid, "neck")[0]; // returns an array, grabs the first index to grab the object
    var lArm = this.extractNamedNodes(humanoid, "leftArm")[0]; // returns an array, grabs the first index to grab the object
    var rArm = this.extractNamedNodes(humanoid, "rightArm")[0]; // returns an array, grabs the first index to grab the object
    var rLeg = this.extractNamedNodes(humanoid, "rightLeg")[0]; // returns an array, grabs the first index to grab the object
    var lLeg = this.extractNamedNodes(humanoid, "leftLeg")[0]; // returns an array, grabs the first index to grab the object

    // Setting position of humanoid object to desire pose.
    // Use personal method dtr() to convert degree to rad
    lLeg.transform.setRotation(0, 0, 0);
    lLeg.saveKeyframe(0);
    rLeg.transform.setRotation(0, 0, 0);
    rLeg.saveKeyframe(0);


    humanoid.transform.setPosition(0, 0, 0)
    lArm.transform.setRotation(0, 0, this.dtr(70));
    lArm.saveKeyframe(0);
    rArm.transform.setRotation(0, 0, this.dtr(-70));
    rArm.saveKeyframe(0);

    //--------------------------------------------------- FRAME 30 KeyFrame -------------------------------
    // At Frame 30 for the arms
    lArm.transform.setRotation(THREE.Math.degToRad(-80), 0, this.dtr(70));
    lArm.saveKeyframe(30);
    rArm.transform.setRotation(THREE.Math.degToRad(80), 0, this.dtr(-70));
    rArm.saveKeyframe(30);

    // At frame 30 for the legs
    lLeg.transform.setRotation(THREE.Math.degToRad(40), 0, 0);
    lLeg.saveKeyframe(30);
    rLeg.transform.setRotation(THREE.Math.degToRad(-40), 0, 0);
    rLeg.saveKeyframe(30);
    //------------------------------------------------- END KeyFrame 30 ----------------------------------------

    //--------------------------------------------------- FRAME 60 KeyFrame -------------------------------
    // At Frame 30 for the arms
    var f60 = 60;
    lArm.transform.setRotation(THREE.Math.degToRad(80), 0, this.dtr(70));
    lArm.saveKeyframe(f60);
    rArm.transform.setRotation(THREE.Math.degToRad(-80), 0, this.dtr(-70));
    rArm.saveKeyframe(f60);

    // At frame 30 for the legs
    lLeg.transform.setRotation(THREE.Math.degToRad(-40), 0, 0);
    lLeg.saveKeyframe(f60);
    rLeg.transform.setRotation(THREE.Math.degToRad(40), 0, 0);
    rLeg.saveKeyframe(f60);
    //------------------------------------------------- END KeyFrame 60 ----------------------------------------

    //--------------------------------------------------- FRAME 90 KeyFrame -------------------------------
    // At Frame 30 for the arms
    lArm.transform.setRotation(THREE.Math.degToRad(-80), 0, this.dtr(70));
    lArm.saveKeyframe(90);
    rArm.transform.setRotation(THREE.Math.degToRad(80), 0, this.dtr(-70));
    rArm.saveKeyframe(90);

    // At frame 30 for the legs
    lLeg.transform.setRotation(THREE.Math.degToRad(40), 0, 0);
    lLeg.saveKeyframe(90);
    rLeg.transform.setRotation(THREE.Math.degToRad(-40), 0, 0);
    rLeg.saveKeyframe(90);
    //------------------------------------------------- END KeyFrame 90 ----------------------------------------

    //--------------------------------------------------- FRAME 120 KeyFrame -------------------------------
    // At Frame 30 for the arms
    var f120 = 120;
    lArm.transform.setRotation(THREE.Math.degToRad(80), 0, this.dtr(70));
    lArm.saveKeyframe(f120);
    rArm.transform.setRotation(THREE.Math.degToRad(-80), 0, this.dtr(-70));
    rArm.saveKeyframe(f120);

    // At frame 30 for the legs
    lLeg.transform.setRotation(THREE.Math.degToRad(-40), 0, 0);
    lLeg.saveKeyframe(f120);
    rLeg.transform.setRotation(THREE.Math.degToRad(40), 0, 0);
    rLeg.saveKeyframe(f120);
    //------------------------------------------------- END KeyFrame 120 ----------------------------------------



    humanoid.saveKeyframe(0)
    humanoid.transform.setPosition(0, 0, .5)
    humanoid.saveKeyframe(30)
    humanoid.transform.setPosition(0, 0, 1);
    humanoid.saveKeyframe(60);
    humanoid.transform.setPosition(0, 0, 1.5);
    humanoid.saveKeyframe(90);
    humanoid.transform.setPosition(0, 0, 2);
    humanoid.saveKeyframe(120);




    //  humanoid.getObjectByName("neck").transform.setPositon(0,0,5);
    //  humanoid.saveKeyframe(30);


    // Return the animated humanoid
    return humanoid
  }

  generateOther() {
    // Create a new humanoid object using your factory
    let humanoid = this._humanoidMaker.generateMesh()
    humanoid.name = `Walking ${humanoid.name}`

    // TODO: Make it do something else!

    // Return the animated robot
    return humanoid
  }

  dtr(degree) {
    return THREE.Math.degToRad(degree);
  }
}


// Export the HumanoidFactory class for use in other modules
export default AnimationFactory
