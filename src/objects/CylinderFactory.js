// Import the three.js library and components needed
import * as THREE from 'three'

// Parent class
import MeshFactory from './MeshFactory'

// TODO: If you use this in your humanoid, then copy over your version
// from project 3. If you do not use it, then you may delete it!

/**
 * A class to build cylinder meshes for use with Three.js
 **/
class CylinderFactory extends MeshFactory {
  /**
   * Create a new cylinder Factory object that will use the given subdivision
   * parameter to generate unit cylinders centered at (0, 0, 0) aligned with Y.
   * @param {number} slices The number of subdivisions around the central axis.
   **/
  constructor (slices) {
    super()
    this._count++
    this._name = `Cylinder ${this._count}`
    this._slices = slices || 18
  }

  /**
   * Set the subdivisions around the outside of the cylinder.
   * @param {number} newVal The number of subdivisions around the central axis.
   **/
  set slices (newVal) {
    if (typeof newVal === 'number') {
      this._slices = newVal
    }
  }

  /**
   * Build and return a THREE.Geometry() object representing a cylinder.
   * @override
   **/
  makeObjectGeometry () {
    var cyclGeom = new THREE.Geometry()
    var radius = 1;
    var segments = this._slices;
    var thetaStart = 0;
    var thetaLength = Math.PI * 2;

    cyclGeom.vertices.push(new THREE.Vector3(0, -1, 0)); // base center // index 0
    cyclGeom.vertices.push(new THREE.Vector3(0, 1, 0)); // top center // index 1
    let normals = [
      new THREE.Vector3(0, -1, 0),
      new THREE.Vector3(0, 1, 0)
    ]

    // top verts
    for (var i = 0; i < segments; i++) {
      var segement = thetaStart + i / segments * thetaLength;
      var vert = new THREE.Vector3();
      vert.x = radius * Math.cos(segement);
      vert.z = radius * Math.sin(segement);
      vert.y = 1;

      cyclGeom.vertices.push(vert);
      normals.push(new THREE.Vector3(vert.x, 0, vert.z))
    }

    // bottom verts
    for (var i = 0; i < segments; i++) {
      var segement = thetaStart + i / segments * thetaLength;
      var vert = new THREE.Vector3();
      vert.x = radius * Math.cos(segement);
      vert.z = radius * Math.sin(segement);
      vert.y = -1;

      cyclGeom.vertices.push(vert);
      normals.push(new THREE.Vector3(vert.x, 0, vert.z))
    }

    // For the faces of the base cyclinder
    var topFaces = [];
    var botFaces = [];
    var sideFaces = [];

    var vertCounter = 1;

    // top faces
    for (var i = 0; i < segments; i++) {

      var nextPoint = 3 + i;
      if (nextPoint > segments + 1) {
        nextPoint = 2;
      }

      // we are drawing in counter clockwise per face
      topFaces.push(new THREE.Face3(1, nextPoint, 2 + i, normals[1]));
    }

    // bot faces

    for (var i = 0; i < segments; i++) {

      // segments is used to help jump us to the 2nd sets of vertices
      var vert3 = 2 + segments + i;
      var vert2 = 3 + segments + i;
      // >= cyclGeom.vertices.length
      if (vert2 >= cyclGeom.vertices.length) {
        vert2 = 2 + segments;
        vert3 = cyclGeom.vertices.length - 1;
      }

      // we are drawing in counter clockwise per face 
      // the order is inverse of how we draw top faces so faces are pointing down
      botFaces.push(new THREE.Face3(0, vert3, vert2, normals[0]));
    }

    // for my side faces
    for (var i = 0; i < segments; i++) {

      var vert0 = 2 + i;
      var vert1 = segments + i + 3;
      var vert2 = segments + i + 2;

      var vert3 = vert0;
      var vert4 = vert0 + 1;
      var vert5 = vert1;

      // last side face condition
      if (i == segments - 1) {

        // new attempt
        vert0 = 2;
        vert1 = segments + 2; // 9
        vert2 = segments + 1; // 10

        vert3 = vert1; // 2
        vert4 = segments * 2 + 1; // 17
        vert5 = vert2; // 16 actually 17

        sideFaces.push(new THREE.Face3(vert0, vert1, vert2, [normals[vert0], normals[vert1], normals[vert2]]));
        sideFaces.push(new THREE.Face3(vert3, vert4, vert5, [normals[vert3], normals[vert4], normals[vert5]]));

      } else {

        sideFaces.push(new THREE.Face3(vert0, vert1, vert2, [normals[vert0], normals[vert1], normals[vert2]])); // bot right triangle
        sideFaces.push(new THREE.Face3(vert3, vert4, vert5, [normals[vert3], normals[vert4], normals[vert5]])); // up left triangle
      }
    }

    var mainFaces = [];
    mainFaces.push(...topFaces);
    mainFaces.push(...botFaces);
    mainFaces.push(...sideFaces);

    cyclGeom.faces = mainFaces;
    return cyclGeom;
  }
}

// Export the CylinderFactory class for use in other modules
export default CylinderFactory
