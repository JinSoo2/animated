// Disable type-checking as it reports incorrect errors
// @ts-nocheck

// Parent class
import MeshFactory from './MeshFactory'

// Base geometry factories
// TODO: Import factory classes as needed
import SphereFactory from './SphereFactory'
import CubeFactory from './CubeFactory'
import CylinderFactory from './CylinderFactory'

class HumanoidFactory extends MeshFactory {
  /**
   * Create a new humanoid factory object.
   **/
  constructor() {
    super()
    this._name = 'Humanoid'
  }

  /**
   * Build and return a THREE.Mesh() object representing a bipedal robot.
   * @override
   * @return {THREE.Mesh} A decorated mesh structure for use with MeshWidget
   **/
  generateMesh() {
    // Empty root object for the entire humanoid character
    let root = MeshFactory.generateEmptyNode('Humanoid')


    // Torso, Neck, Head, Two arms, Two hands, two legs, two feet

    // Factories
    var cylinderFactory = new CylinderFactory(6);
    var cubeFactory = new CubeFactory();
    var sphereFactory = new SphereFactory(6, 6);

    // Torso
    var torso = cubeFactory.generateMesh();
    torso.name = "torso";
    torso.transform.setPivot(0, 0, 0);
    torso.transform.setPosition(0, 0, 0);
    torso.transform.setScale(0.7, 1.0, 0.5);
    torso = MeshFactory.isolateScale(torso);
    root.add(torso);

    // neck
    var neck = cylinderFactory.generateMesh();
    neck.name = 'neck';
    neck.transform.setPosition(0, 1.3, 0);
    neck.transform.setScale(.3, .3, .3);
    neck = MeshFactory.isolateScale(neck);
    neck.transform.setPivot(0, .3, 0);
    torso.add(neck);

    // head
    sphereFactory.slices = 20;
    sphereFactory.stacks = 20;
    var head = sphereFactory.generateMesh();
    head.name = 'head';
    head.transform.setPosition(0, 1, 0);
    neck.add(head);

    // left arm - avoid rotations in arm
    var leftArm = cubeFactory.generateMesh();
    leftArm.name = "leftArm";
    leftArm.transform.setScale(1, .2, .2);
    leftArm.transform.setPosition(-1.7, 0.7, 0);
    leftArm.transform.setPivot(1, 0, 0);
    leftArm = MeshFactory.isolateScale(leftArm);
    torso.add(leftArm);

    // left hand
    sphereFactory.slices = 8;
    sphereFactory.stacks = 8;
    var leftHand = sphereFactory.generateMesh();
    leftHand.name = "leftHand";
    leftHand.transform.setPosition(-1, 0, 0);
    leftHand.transform.setScale(.4, .4, .4);
    leftArm.add(leftHand);

    // right arm - avoid rotations in arm
    var rightArm = cubeFactory.generateMesh();
    rightArm.name = "rightArm";
    rightArm.transform.setScale(1, .2, .2);
    rightArm.transform.setPosition(1.7, 0.7, 0);
    rightArm.transform.setPivot(-1, 0, 0);
    rightArm = MeshFactory.isolateScale(rightArm);
    torso.add(rightArm);

    // right hand
    sphereFactory.slices = 8;
    sphereFactory.stacks = 8;
    var rightHand = sphereFactory.generateMesh();
    rightHand.name = "leftHand";
    rightHand.transform.setPosition(1, 0, 0);
    rightHand.transform.setScale(.4, .4, .4);
    rightArm.add(rightHand);

    // right leg
    var rightLeg = cubeFactory.generateMesh();
    rightLeg.name = "rightLeg";
    rightLeg.transform.setScale(.2, 1, .2);
    rightLeg.transform.setPosition(.4, -2, 0);
    rightLeg.transform.setPivot(0, 1, 0);
    rightLeg = MeshFactory.isolateScale(rightLeg);
    torso.add(rightLeg);

    // right feet
    sphereFactory.slices = 8;
    sphereFactory.stacks = 8;
    var rightFeet = sphereFactory.generateMesh();
    rightFeet.name = "rightFeet";
    rightFeet.transform.setPosition(0, -1.25, 0);
    rightFeet.transform.setScale(.35, .35, .35);
    rightLeg.add(rightFeet);

    // left leg - avoid rotations in arm
    var leftLeg = cubeFactory.generateMesh();
    leftLeg.name = "leftLeg";
    leftLeg.transform.setScale(.2, 1, .2);
    leftLeg.transform.setPosition(-.4, -2, 0);
    leftLeg.transform.setPivot(0, 1, 0);
    leftLeg = MeshFactory.isolateScale(leftLeg);
    torso.add(leftLeg);

    // left feet
    sphereFactory.slices = 8;
    sphereFactory.stacks = 8;
    var leftFeet = sphereFactory.generateMesh();
    leftFeet.name = "leftFeet";
    leftFeet.transform.setPosition(0, -1.25, 0);
    leftFeet.transform.setScale(.35, .35, .35);
    leftLeg.add(leftFeet);


    // move the root up so the feet are on ground
    root.transform.setPosition(0, 0.5, 0);
    root.transform.setScale(0.33, 0.33, 0.33);
    // Return the completed mesh
    return root;
  }
}

// Export the HumanoidFactory class for use in other modules
export default HumanoidFactory
