// Import the three.js library and components needed
import * as THREE from 'three'

// Parent class
import MeshFactory from './MeshFactory'

// TODO: If you use this in your humanoid, then copy over your version
// from project 3. If you do not use it, then delete it!

/**
 * A class to build sphere meshes for use with Three.js
 **/
class SphereFactory extends MeshFactory {
  /**
   * Create a new sphere factory object that will use the given subdivision
   * parameters to generate unit spheres centered at (0, 0, 0).
   * @param {number} slices The number of subdivisions around the equator.
   * @param {number} stacks The number of subdivisions between the poles.
   **/
  constructor (slices, stacks) {
    super()
    this._count++
    this._name = `Sphere ${this._count}`
    this._slices = slices || 36
    this._stacks = stacks || 18
  }

  /**
   * Set the subdivisions around the equator of the sphere.
   * @param {number} newVal The number of subdivisions around the equator.
   **/
  set slices (newVal) {
    if (typeof newVal === 'number') {
      this._slices = newVal
    }
  }

  /**
   * Set the subdivisions between the poles of the sphere.
   * @param {number} newVal The number of subdivisions between the poles.
   **/
  set stacks (newVal) {
    if (typeof newVal === 'number') {
      this._stacks = newVal
    }
  }

  /**
   * Build and return a THREE.Geometry() object representing a sphere.
   * @override
   **/
  makeObjectGeometry () {
    var sphereGeo = new THREE.Geometry();

    // var stacks = this.stacks;
    // // calculate the phi-increment
    // var phiIncr = Math.PI / stacks;
    // // calculate theta-increment 
    // var thetaIncr = 2 * Math.PI / stacks

    // var phi = phiIncr;
    //----------------------------------------------------------------------------------------------------------------
    var radius = 1;

    // stack
    var widthSegments = this._slices;
    // slice
    var heightSegments = this._stacks;

    var phiStart = 0;
    var phiLength = Math.PI * 2;

    var thetaStart = 0;
    var thetaLength = Math.PI;
    var thetaEnd = thetaStart + thetaLength;

    // vertex index counter
    var index = 0;

    // to store vertices as a grid for later easier accessing
    var vertexGrid = [];
    var vertex = new THREE.Vector3();

    var faces = [];
    var vertices = [];

    // create vertices
    var x;
    var y;
    for (y = 0; y <= heightSegments; y++) {

      var verticesRow = [];

      var v = y / heightSegments;

      for (x = 0; x <= widthSegments; x++) {

        var u = x / widthSegments;

        // vertex
        vertex.x = - radius * Math.cos(phiStart + u * phiLength) * Math.sin(thetaStart + v * thetaLength);
        vertex.y = radius * Math.cos(thetaStart + v * thetaLength);
        vertex.z = radius * Math.sin(phiStart + u * phiLength) * Math.sin(thetaStart + v * thetaLength);
        vertices.push(new THREE.Vector3(vertex.x, vertex.y, vertex.z));
        verticesRow.push(index++);

      }

      vertexGrid.push(verticesRow);

    }

    // indices
    var counter = 0;

    var verts = vertices;
    for (y = 0; y < heightSegments; y++) {

      for (x = 0; x < widthSegments; x++) {

        var a = vertexGrid[y][x + 1];
        var b = vertexGrid[y][x];
        var c = vertexGrid[y + 1][x];
        var d = vertexGrid[y + 1][x + 1];

        if (y !== 0 || thetaStart > 0) {
          let norms = [verts[a], verts[b], verts[d]];
          faces.push(new THREE.Face3(a, b, d, norms));
        }

        if (y !== heightSegments - 1 || thetaEnd < Math.PI) {
          let norms = [verts[b], verts[c], verts[d]];
          faces.push(new THREE.Face3(b, c, d, norms));
        }

      }

    }

    // ... spread operator that allow you to push elements in one array to another array,
    // without having to use a for loop
    // sphereGeo.vertices.push(...vertices);
    // sphereGeo.faces.push(...faces);
    vertices.forEach(vertex => {
      sphereGeo.vertices.push(vertex);
    });

    faces.forEach(face => {
      sphereGeo.faces.push(face);
    });

    return sphereGeo
  }
}

// Export the SphereFactory class for use in other modules
export default SphereFactory
